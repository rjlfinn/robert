// Qub Final Year Project

#pragma once

// Netork Headers
#include "Net/UnrealNetwork.h"

#include "Public/Math/Transform.h"
#include "GameFramework/Actor.h"
#include <Containers/Array.h>

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ReplicateTransform.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UReplicateTransform : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UReplicateTransform();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;

	//bool Successful;
	//void SetBool(bool NewBool);
	//UFUNCTION(reliable, server, WithValidation);
	//void ServerSetBool(bool NewBool);

private:
	// Actor owner
	AActor * Owner;

	// Replicate Property over Network
	UPROPERTY(replicated)
	FVector Location;
};
