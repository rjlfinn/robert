// Qub Final Year Project

#pragma once

#include <LeapFrame.h>
#include <LeapController.h>
#include <LeapMotionClasses.h>
#include <Leap.h>
#include <LeapMotionClasses.h>
#include <ILeapMotion.h>

#include <LeapTool.h>

#include <LeapHandList.h>
#include <LeapHand.h>
#include <LeapFingerList.h>
#include <LeapFinger.h>
#include <LeapBone.h>
#include <LeapArm.h>

#include <LeapGestureList.h>

#include <AnimBody/AnimBone.h>
#include <Containers/Array.h>
#include <Components/PoseableMeshComponent.h>
#include "GameFramework/Actor.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PoseableHand.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BUILDINGESCAPE_API UPoseableHand : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UPoseableHand();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	// Store Bone Transformations for Each Hands
	void StoreLeftHandTransformations();
	void StoreRightHandTransformations();

	// Set Bone Transformations for PoseableMesh
	void SetLeftHandTransformations();
	void SetRightHandTransformations();

	// Change Coordindate Systems
	void LeapVectorToUnreal(FVector Vector);

	// Check for Hand Gestures
	void CheckGestures();

	const int NumFingers = 5;
	const int NumBonesPerFinger = 4;

	// Actor owner
	AActor* Owner;

	// Poseable mesh component - used to manipulate bone transformations
	UPoseableMeshComponent* PoseableMesh = nullptr;

	// Leap Controller used to access Frames
	ULeapController* Controller = nullptr;

	// Most recent Frame of data from Leap motion controller
	ULeapFrame* CurrentFrame = nullptr;

	// Store Most Recent Hand information
	ULeapHandList* Hands = nullptr;
	ULeapHand* LeftHand = nullptr;
	ULeapHand* RightHand = nullptr;

	// Bone names in hand 
	FName Forearm = "Bip01-R-Forearm";
	FName Wrist = "Bip01-R-Hand";
	TArray<FName> Metacarpels = { "Bip01-R-Finger0", "Bip01-R-Finger1", "Bip01-R-Finger2" , "Bip01-R-Finger3", "Bip01-R-Finger4" };
	TArray<FName> Proximals = { "Bip01-R-Finger01", "Bip01-R-Finger11", "Bip01-R-Finger21" , "Bip01-R-Finger31", "Bip01-R-Finger41" };
	TArray<FName> Intermediates = { "Bip01-R-Finger02", "Bip01-R-Finger12", "Bip01-R-Finger22" , "Bip01-R-Finger32", "Bip01-R-Finger42" };
	TArray<FName> Distals = { "Bip01-R-Finger0Nub", "Bip01-R-Finger1Nub", "Bip01-R-Finger2Nub" , "Bip01-R-Finger3Nub", "Bip01-R-Finger4Nub" };

	//FName Forearm = "rt_forearm_roll_c";
	//FName Wrist = "rt_wrist";
	//TArray<FName> Metacarpels = { "rt_thumb_meta", "rt_index_meta", "rt_middle_meta" , "rt_ring_meta", "rt_pinky_meta" };
	//TArray<FName> Proximals = { "rt_thumb_a", "rt_index_a", "rt_middle_a" , "rt_ring_a", "rt_pinky_a" };
	//TArray<FName> Intermediates = { "rt_thumb_b", "rt_index_b", "rt_middle_b" , "rt_ring_b", "rt_pinky_b" };
	//TArray<FName> Distals = { "rt_thumb_c", "rt_index_c", "rt_middle_c" , "rt_ring_c", "rt_pinky_c" };

	// Transformations of bones
	FTransform ForearmTransformation[1];
	FTransform WristTransformation[1];
	FTransform LeftMetacarpelTransformations[5];
	FTransform LeftProximalTransformations[5];
	FTransform LeftIntermediateTransformations[5];
	FTransform LeftDistalTransformations[5];

	FTransform RightForearmTransformations[1];
	FTransform RightMetacarpelTransformations[5];
	FTransform RightProximalTransformations[5];
	FTransform RightIntermediateTransformations[5];
	FTransform RightDistalTransformations[5];
};
