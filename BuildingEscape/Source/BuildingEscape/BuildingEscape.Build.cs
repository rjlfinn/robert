// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class BuildingEscape : ModuleRules
{
	public BuildingEscape(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "LeapMotion"});

		PrivateDependencyModuleNames.AddRange(new string[] {  });

        PublicIncludePaths.AddRange(new string[] { "LeapMotion/ThirdParty/LeapSDK/Include/" });
        PublicIncludePaths.AddRange(new string[] { "LeapMotion/Public", "LeapMotion/Classes", "LeapMotion/Public" });
        PrivateIncludePaths.AddRange(new string[] { "LeapMotion/Public", "LeapMotion/Classes", "LeapMotion/Private" });

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
