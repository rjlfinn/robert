// Qub Final Year Project

#pragma once
#include <LeapFrame.h>
#include <LeapController.h>
#include "CoreMinimal.h"

using namespace Leap;

/**
 * 
 */
class BUILDINGESCAPE_API LeapEventListener : public Listener
{
public:
	LeapEventListener();
	~LeapEventListener();

	virtual void someMethod();
	//void onConnect(const Controller&);
	//void onDisconnect(const Controller&);
	//void onFrame(const Controller&);
};

void LeapEventListener::someMethod()
{

}
//void LeapEventListener::onConnect(const Controller& controller) {
//	std::cout << "Connected" << std::endl;
//	// Enable gestures, set Config values:
//	controller.enableGesture(Gesture::TYPE_SWIPE);
//	controller.config().setFloat("Gesture.Swipe.MinLength", 200.0);
//	controller.config().save();
//}
//
////Not dispatched when running in a debugger
//void LeapEventListener::onDisconnect(const Controller& controller) {
//	std::cout << "Disconnected" << std::endl;
//}
//
//void LeapEventListener::onFrame(const Controller& controller) {
//	std::cout << "New frame available" << std::endl;
//	Frame frame = controller.frame();
//	// Process this frame's data...
//}