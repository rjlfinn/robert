// Qub Final Year Project

#include "PoseableHand.h"


// Sets default values for this component's properties
UPoseableHand::UPoseableHand()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game startss
void UPoseableHand::BeginPlay()
{
	Super::BeginPlay();

	// Get Actor Owner
	Owner = GetOwner();

	// Get Poseable Mesh Component
	PoseableMesh = Owner->FindComponentByClass<UPoseableMeshComponent>();

	if (!PoseableMesh)
	{
		UE_LOG(LogTemp, Error, TEXT("%s cannot find Poseable Mesh component"), *Owner->GetName());
	}

	// Set Leap Controller
	Controller = Owner->FindComponentByClass<ULeapController>();

	if (!Controller)
	{
		UE_LOG(LogTemp, Error, TEXT("%s cannot find Leap Controller component"), *Owner->GetName());
	}

	// Enable Gesture Feature
	Controller->EnableGesture(LeapGestureType::GESTURE_TYPE_CIRCLE);
	Controller->EnableGesture(LeapGestureType::GESTURE_TYPE_KEY_TAP);
	Controller->EnableGesture(LeapGestureType::GESTURE_TYPE_SWIPE);
}

// Called every frame
void UPoseableHand::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UE_LOG(LogTemp, Warning, TEXT("Output"));

	if (Controller->IsConnected())
	{
		CurrentFrame = Controller->Frame(0);
		Hands = CurrentFrame->Hands();

		// For each hand
		for (int h = 0; h < Hands->Count; h++)
		{
			ULeapHand* Hand = CurrentFrame->Hands()->Frontmost();
			if (Hand->IsLeft)
			{
				UE_LOG(LogTemp, Warning, TEXT("Left Hand\n"));
				LeftHand = Hand;
				StoreLeftHandTransformations();
				SetLeftHandTransformations();
			}
			else if (Hand->IsRight)
			{
				UE_LOG(LogTemp, Warning, TEXT("Right Hand\n"));
				RightHand = Hand;
				StoreRightHandTransformations();
				SetRightHandTransformations();
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Hand not recognised \n"));
			}

			CheckGestures();
		}
	}
	
}

void UPoseableHand::StoreLeftHandTransformations()
{
	/*
	* Access each finger on hand:
	* The Order in which they are accessed:
	*		- Thumb, Index, Middle, Ring, Pinky
	*/
	ULeapFingerList* Fingers = LeftHand->Fingers();
	for (int f = 0; f < Fingers->Count; f++)
	{
		ULeapFinger* Finger = Fingers->GetPointableById(f);

		/*
		* For each bone in finger record bone transformation.
		* The order is:
		*		- Metacarpels, Proximals, Intermediates, Distals
		*/
		for (int b = 0; b < NumBonesPerFinger; b++)
		{
			LeapBoneType Type = (LeapBoneType)b;
			ULeapBone* CurrentBone = Finger->Bone(Type);

			FTransform BoneTransform(CurrentBone->Basis);
			switch (Type)
			{
			case LeapBoneType::TYPE_METACARPAL:
				LeftMetacarpelTransformations[f] = BoneTransform;
				break;
			case LeapBoneType::TYPE_PROXIMAL:
				LeftProximalTransformations[f] = BoneTransform;
				break;
			case LeapBoneType::TYPE_INTERMEDIATE:
				LeftIntermediateTransformations[f] = BoneTransform;
				break;
			case LeapBoneType::TYPE_DISTAL:
				LeftDistalTransformations[f] = BoneTransform;
				break;
			case LeapBoneType::TYPE_ERROR:
				break;
			}
		}
	}
}

void UPoseableHand::StoreRightHandTransformations()
{
	/*
	* Access each finger on hand:
	* The Order in which they are accessed:
	*		- Thumb, Index, Middle, Ring, Pinky
	*/
	ULeapFingerList* Fingers = RightHand->Fingers();
	for (int f = 0; f < Fingers->Count; f++)
	{
		ULeapFinger* Finger = Fingers->GetPointableById(f);

		/*
		* For each bone in finger record bone transformation.
		* The order is:
		*		- Metacarpels, Proximals, Intermediates, Distals
		*/
		LeapBoneType Type;
		ULeapBone* CurrentBone;
		FTransform BoneTransform;

		// Set Forearm Bone Transform
		BoneTransform.SetIdentity();
		FVector LeapLocation = RightHand->Arm->ElbowPosition;
		FVector Location(LeapLocation.Y, -LeapLocation.X, LeapLocation.Z);
		BoneTransform.SetTranslation(Location);

		FRotator ForearmRotation(RightHand->Arm->Basis.Rotator());
		ForearmRotation.Yaw = -90 + ForearmRotation.Yaw;
		ForearmRotation.Roll = -135 + ForearmRotation.Roll;
		BoneTransform.SetRotation(ForearmRotation.Quaternion());

		ForearmTransformation[0] = BoneTransform;

		BoneTransform.SetIdentity();
		FVector Wrist = RightHand->Arm->WristPosition;
		BoneTransform.SetTranslation(FVector(Wrist.Y, -Wrist.X, Wrist.Z));

		FRotator WristRotation(RightHand->Basis.Rotator());
		WristRotation.Yaw = -90 + WristRotation.Yaw;
		WristRotation.Roll = -90 + WristRotation.Roll;
		BoneTransform.SetRotation(WristRotation.Quaternion());
		WristTransformation[0] = BoneTransform;

		for (int b = 0; b < 4; b++)
		{
			Type = (LeapBoneType)b;
			CurrentBone = Finger->Bone(Type);

			if (CurrentBone->IsValid)
			{

				BoneTransform.SetIdentity();

				//FVector BoneScale = CurrentBone->Length
				FRotator BoneRotation(CurrentBone->Basis.Rotator());
				FVector BoneLocation = CurrentBone->Center;

				BoneRotation.Yaw = -90 + BoneRotation.Yaw;
				BoneRotation.Roll = -90 + BoneRotation.Roll;
				BoneTransform.SetRotation(BoneRotation.Quaternion());

				//BoneTransform = BoneRotation;
				BoneTransform.SetRotation(BoneRotation.Quaternion());
				BoneTransform.SetLocation(FVector(BoneLocation.Y, -BoneLocation.X, BoneLocation.Z));
				
				switch (Type)
				{
				case LeapBoneType::TYPE_METACARPAL:
					RightMetacarpelTransformations[f] = BoneTransform;
					break;
				case LeapBoneType::TYPE_PROXIMAL:
					RightProximalTransformations[f] = BoneTransform;
					break;
				case LeapBoneType::TYPE_INTERMEDIATE:
					RightIntermediateTransformations[f] = BoneTransform;
					break;
				case LeapBoneType::TYPE_DISTAL:
					RightDistalTransformations[f] = BoneTransform;
					break;
				case LeapBoneType::TYPE_ERROR:
					break;
				}
			}
		}
	}
}

void UPoseableHand::SetLeftHandTransformations()
{

}

void UPoseableHand::SetRightHandTransformations()
{
	UE_LOG(LogTemp, Warning, TEXT("Setting Bone Transformations Right Hand \n"));

	PoseableMesh->SetBoneLocationByName(Forearm, ForearmTransformation[0].GetLocation(), EBoneSpaces::ComponentSpace);
	PoseableMesh->SetBoneRotationByName(Forearm, FRotator(ForearmTransformation[0].GetRotation()), EBoneSpaces::ComponentSpace);
	PoseableMesh->SetBoneLocationByName(Wrist, WristTransformation[0].GetLocation(), EBoneSpaces::ComponentSpace);
	PoseableMesh->SetBoneRotationByName(Wrist, FRotator(WristTransformation[0].GetRotation()), EBoneSpaces::ComponentSpace);

	for (int b = 0; b < NumFingers; b++)
	{
		PoseableMesh->SetBoneLocationByName(Metacarpels[b], RightMetacarpelTransformations[b].GetLocation(), EBoneSpaces::ComponentSpace);
		PoseableMesh->SetBoneLocationByName(Proximals[b], RightProximalTransformations[b].GetLocation(), EBoneSpaces::ComponentSpace);
		PoseableMesh->SetBoneLocationByName(Intermediates[b], RightIntermediateTransformations[b].GetLocation(), EBoneSpaces::ComponentSpace);
		PoseableMesh->SetBoneLocationByName(Distals[b], RightDistalTransformations[b].GetLocation(), EBoneSpaces::ComponentSpace);

		PoseableMesh->SetBoneRotationByName(Metacarpels[b], FRotator(RightMetacarpelTransformations[b].GetRotation()), EBoneSpaces::ComponentSpace);
		PoseableMesh->SetBoneRotationByName(Proximals[b], FRotator(RightProximalTransformations[b].GetRotation()), EBoneSpaces::ComponentSpace);
		PoseableMesh->SetBoneRotationByName(Intermediates[b], FRotator(RightIntermediateTransformations[b].GetRotation()), EBoneSpaces::ComponentSpace);
		PoseableMesh->SetBoneRotationByName(Distals[b], FRotator(RightDistalTransformations[b].GetRotation()), EBoneSpaces::ComponentSpace);

	}
}

void UPoseableHand::CheckGestures()
{
	ULeapGestureList* gestures = CurrentFrame->Gestures();

	UE_LOG(LogTemp, Warning, TEXT("Number of Gestures: %d "), CurrentFrame->Gestures()->Count);


	for (int g = 0; g < gestures->Count; g++)
	{

		// TODO: Spawn Particles from hand depending on gesture being made
		if (CurrentFrame->Gesture(g)->Type == LeapGestureType::GESTURE_TYPE_CIRCLE)
		{
			UE_LOG(LogTemp, Warning, TEXT("Circle: "));
		}
	}
}

