// Qub Final Year Project

#pragma once
#include "Modules/ModuleManager.h"
#include "GameFramework/Actor.h"
#include "UObject/NoExportTypes.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ReportObjectPosition.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UReportObjectPosition : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UReportObjectPosition();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
