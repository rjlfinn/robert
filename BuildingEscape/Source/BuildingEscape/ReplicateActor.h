// Qub Final Year Project

#pragma once
#include "Net/UnrealNetwork.h"

#include "Public/Math/Transform.h"
#include "GameFramework/Actor.h"
#include <Containers/Array.h>

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ReplicateActor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UReplicateActor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UReplicateActor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;

	// Actor owner
	UPROPERTY(replicated)
	AActor *Owner;
};
