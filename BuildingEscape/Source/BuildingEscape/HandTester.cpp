// Qub Final Year Project

#include "HandTester.h"


// Sets default values for this component's properties
UHandTester::UHandTester()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHandTester::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHandTester::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	Leap::Controller controller;

	Leap::Frame frame = controller.frame();
	Leap::HandList hands = frame.hands();

	Leap::Hand frontHand = frame.hands().frontmost();
	Leap::Vector frontHandPosition = frontHand.palmPosition();
	FString velocity = UTF8_TO_TCHAR(frontHandPosition.toString().c_str());


	//UE_LOG(LogTemp, Warning, TEXT("Velocity on %s"), *velocity);

	AActor* Owner = GetOwner();
	FVector position = Owner->GetTransform().GetLocation();
	//velocity = LEAPVECTOR_TO_FVECTOR;
	FVector NewPosition = FVector((frontHandPosition.x / 10), 10 + (frontHandPosition.y / 10), (frontHandPosition.z / 10));

	//Owner->SetActorLocation(NewPosition);

	Leap::Finger finger = frontHand.finger(0);
	Leap::Bone bone = finger.bone(Leap::Bone::Type::TYPE_METACARPAL);
	

	// Get Skeletal Mesh Component
	TArray<USkeletalMeshComponent*> Components;
	Owner->GetComponents<USkeletalMeshComponent>(Components);

	//UE_LOG(LogTemp, Warning, TEXT("Num skeletal components: %d"), Components.Num());

	for (int32 i = 0; i < Components.Num(); i++)
	{
		USkeletalMeshComponent* SkeletalMesh = Components[i];
		//UE_LOG(LogTemp, Warning, TEXT("Num skeletal components: %d"), SkeletalMesh->GetNumBones());
		FName BoneName = SkeletalMesh->GetBoneName(26);
		
		
		//UE_LOG(LogTemp, Warning, TEXT("Bone Name: %s"), *BoneName.ToString());

		FName RootBoneName("Bip01-R-ForeTwist");
		FBodyInstance* Root = SkeletalMesh->GetBodyInstance(RootBoneName, false);
	

		int32 RootIndex = SkeletalMesh->GetBoneIndex("Bip01-R-ForeTwist");
		FVector RootTranslation = SkeletalMesh->BoneSpaceTransforms[RootIndex].GetLocation();
		FQuat InitialRotation = SkeletalMesh->BoneSpaceTransforms[RootIndex].GetRotation();
		SkeletalMesh->BoneSpaceTransforms[RootIndex].SetRotation(FQuat(0.45, 0, 0, 0.090));
		SkeletalMesh->BoneVisibilityStates[RootIndex] = 1;
		//SkeletalMesh->RecalcRequiredBones(RootIndex);
		//SkeletalMesh->UpdateBoneBodyMapping();
		//SkeletalMesh->BoneSpaceTransforms[PinkyEndBone].SetLocation(FVector(5, 59, 2));
		//SkeletalMesh->SetBone
		

		//SkeletalMesh->RefreshBoneTransforms();
		//UE_LOG(LogTemp, Warning, TEXT("Bone T1: %s"), *SkeletalMesh->BoneSpaceTransforms[RootIndex].GetRotation().ToString());
		//SkeletalMesh->GetBodyInstance("Bip01-R-FingerONub")->Set
	}

	//TArray<UStaticMeshComponent*> Components;
	//Owner->GetComponents<UStaticMeshComponent>(Components);

	//for (int32 i = 0; i < Components.Num(); i++)
	//{
	//	UStaticMeshComponent* StaticMeshComponent = Components[i];
	//	UStaticMesh* StaticMesh = StaticMeshComponent->GetStaticMesh();
	//}

	//bone.


	

	// ...
}

