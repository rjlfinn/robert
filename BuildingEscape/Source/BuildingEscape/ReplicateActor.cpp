// Qub Final Year Project

#include "ReplicateActor.h"


// Sets default values for this component's properties
UReplicateActor::UReplicateActor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	// Replicate Set to True
	bReplicates = true;
}


// Called when the game starts
void UReplicateActor::BeginPlay()
{
	Super::BeginPlay();

	// Get Actor Owner
	Owner = GetOwner();

}


// Called every frame
void UReplicateActor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UReplicateActor::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to Everyone
	DOREPLIFETIME(AActor, Owner);
}

