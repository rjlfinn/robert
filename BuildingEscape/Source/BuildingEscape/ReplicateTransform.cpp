// Qub Final Year Project

#include "ReplicateTransform.h"


// Sets default values for this component's properties
UReplicateTransform::UReplicateTransform()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Replicate Set to True
	bReplicates = true;

}


// Called when the game starts
void UReplicateTransform::BeginPlay()
{
	Super::BeginPlay();

	// Get Actor Owner
	Owner = GetOwner();

	// Set Initial Location
	Location = Owner->GetActorLocation();

}


// Called every frame
void UReplicateTransform::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	// Get Transform component and initalise location
	if (Location != Owner->GetActorLocation())
	{
		Location = Owner->GetActorLocation();
	}
	
	UE_LOG(LogTemp, Warning, TEXT("Location: %s"), *Location.ToString());

}

void UReplicateTransform::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to Everyone
	DOREPLIFETIME(UReplicateTransform, Location);
}
//
//void UReplicateTransform::SetBool(bool NewBool)
//{
//	Successful = NewBool;
//
//	if (Role < ROLE_Authority)
//	{
//		// Client
//		ServerSetBool(NewBool);
//	}
//}
//
//void UReplicateTransform::ServerSetBool_Validate(bool NewBool)
//{
//	return true;
//}
//
//void UReplicateTransform::ServerSetBool_Implementation(bool NewBool)
//{
//	SetSomeBool(NewBool);
//}
//
